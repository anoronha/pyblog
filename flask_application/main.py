from flask import Flask,render_template
from flask_application import app
from services import blogservice

@app.route('/')
def run_app():
    model = {'name':'arun'}
    template = render_template('index.html', model=model)
    return template

@app.route('/blog')
def get_blogs():
    model = blogservice.Blog().get_blogs()
    template = render_template('blogList.html', model=model)
    return template